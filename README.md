# Local Youtube Subscription Box

I noticed a year ago that I waste way too much time on Youtube watching pointless videos suggested by Youtube's cunning algorithm. I wanted to stop it but not give up on Youtube completely. Thus I wrote lysb which lets me follow my favorite youtubers without having to use the Youtube itself directly. Now I have more time to browse imageboards, all thanks to this wonderful program.

Mind you, this program doesn't rely on the official API provided by Youtube so it might break down any day. This method has been working for over a year now so there's no plan on changing anything.

Now you can add playlists to subfile. This is very handy if you're only interested in specific content and the youtuber has made a playlist for that.

# Requirements
* Python 3.5
* Requests module
* Youtube-dl module

# how-to

### Check status:

`python main.py --sub-file <subscription file>`


### Subscribe:

`python main.py --sub-file <subscription file> --sub <channel hash>`

`python main.py --sub-file <subscription file> --sub <channel url>`

`python main.py --sub-file <subscription file> --sub <user url>`

`python main.py --sub-file <subscription file> --sub <playlist url>`


You can also add many channels/playlists at once:

`python main.py --sub-file <subscription file> --sub <user1 url> <channel2 url> <channel3 hash> <playlist 666>`

for example:

`python main.py --sub-file subfile.txt --sub "https://www.youtube.com/user/Computerphile" "https://www.youtube.com/channel/UCAL3JXZSzSm8AlZyD3nQdBA" "UCoxcjq-8xIDTYp3uz647V5A" "https://www.youtube.com/playlist?list=PLrxfgDEc2NxY_fRExpDXr87tzRbPCaA5x"`


### Unsubscribe: 

`python main.py --sub-file <subscription file> --unsub <user1 url> <channel2 url> <channel3 hash> <playlists 4>`


# Copying
lysb is free software. It's licensed under the MIT License.
