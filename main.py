#!/usr/bin/env python
# encoding: utf-8

import sys, os, requests, re, youtube_dl, argparse, subprocess

# Fetch channel hash from Youtube
def get_channel_hash(url):
    print("[+] Fetching channel hash...")
    data = requests.get(url).text
    try:
        return re.findall(r'data-channel-external-id=\"(.*?)\"', data)[0]
    except:
        return None

def get_playlist_hash(url):
    return url.split("=")[1]

def rewrite_subfile(subfile, subs):
    subinfo = ";".join(sub for sub in subs if sub != "")
    subfile.seek(0)
    subfile.write(subinfo)
    subfile.truncate()

# Remove channels from subfile (same as unsubscribing)
def unsub(infile, channels):
    with open(infile, "r+") as subfile:
        hash_list = []
        for channel in channels:
            if len(channel) == 24 and channel[0:2] == "UC":
                hash_list.append(channel)
            else:
                hash_list.append(get_channel_hash(channel))

        subs = subfile.read().split(";")
        for i, sub in enumerate(subs):
            if sub.split(" ")[0] in hash_list:
                subs[i] = ""

        rewrite_subfile(subfile, subs)

# Dowload all new videos. Returns the id of newest video
def get_new_videos(cur_id, ids):
    all_vids = [ids[i:i+11] for i in range(0, len(ids), 11)]
    if not all_vids:
        print("[-] Video list not found, aborting...")
        return None

    if cur_id not in all_vids:
        print("[-] Latest video has been deleted, aborting...{0} {1}".format(cur_id, all_vids))
        return all_vids[0]

    ydl_opts = {
        'ignoreerrors': True,
        'retries': 1000,
    }
    ydl = youtube_dl.YoutubeDL(ydl_opts)
    for vid in all_vids:
        if vid != cur_id:
            vid_url = "https://youtube.com/watch?v={0}".format(vid)
            with ydl:
                print("[+] Downloading video: {0}...".format(vid_url))
                if ydl.download([vid_url]) == 1:
                    print("Using subprocess.call() as fallback...")
                    subprocess.call(["youtube-dl", vid_url])
        else:
            break
    return all_vids[0]


# Download and return a list containing 30 most recent videos
def get_channel_videos(url):
    print("[+] Downloading video list...")

    data = requests.get(url).text
    user = re.findall(r'title\":\"(.*?)\"', data)[0].split("-")[0][:-1]
    print("[+] Checking user: {0}...".format(user))

    m = re.findall(r'watch\?v=.{11}', data)
    vids = "".join([m[i].split("=")[1] for i in range(0, len(m), 2)])
    return vids

# this function is a little harder than the get_playlist_videos()
# because the list youtube provides for channel's videos is always
# in order (newest video first) but this MIGHT not be the case for playlists.
# They can be order either first or last video first. We want last video
# first meaning that we must check here which is the newer video
# (first item in the list or the last)
#
# If the order is first video first, we must reverse the order of the list
def get_playlist_videos(url):
    print("[+] Downloading video list...")

    data = requests.get(url).text
    m = re.findall(r'data-video-id=\"[^\"]+\"', data)
    vids = [m[i].split("=")[1][1:-1] for i in range(0, len(m))]

    first_vid_ud = ""
    last_vid_ud = ""

    with youtube_dl.YoutubeDL() as ydl:
        first_info_dict = ydl.extract_info(vids[0], download=False)
        last_info_dict  = ydl.extract_info(vids[-1],  download=False)
        first_vid_ud = first_info_dict.get('upload_date', None)
        last_vid_ud  = last_info_dict.get('upload_date', None)

    if first_vid_ud < last_vid_ud:
        vids.reverse()

    return "".join(vid for vid in vids)


# Check if any of the channels have uploaded new videos.
# If they have, dowload them and store the id of latest video to subfile
def check_subbox_status(infile):
    with open(infile, "r+") as subfile:
        subs = subfile.read().split(';')
        for i, sub in enumerate(subs):
            if sub[0:2] == "UC":
                channel_url = "https://www.youtube.com/channel/" + sub[:24]
                latest_vid  = sub[25:36]
                vids = get_channel_videos(channel_url + "/videos?spf=navigate-back")
            else:
                channel_url = "https://www.youtube.com/playlist?list=" + sub[:34]
                latest_vid  = sub[35:46]
                vids = get_playlist_videos(channel_url)

            if latest_vid != vids[0:11]:
                print("[+] New videos available, starting download...")
                new_vid = get_new_videos(latest_vid, vids)

                if new_vid:
                    subs[i] = sub.replace(latest_vid, new_vid)
                else:
                    print("[-] Unable to download video list! Maybe this channel has been deleted?\n"
                          "[-] Channel url: {0}".format(channel_url))
            else:
                print("[+] Subbox up to date!")

        rewrite_subfile(subfile, subs)

# Append new channels at the end of subfile or create new one if subfile doesn't exist
def sub(infile, channels):
    with open(infile, "a") as subfile:
        channel_info = ""
        for channel in channels:
            channel_hash = ""
            chan_type = ""
            if len(channel) == 24 and channel[0:2] == "UC":
                channel_hash = channel
                chan_type = "channel"
            elif len(channel) == 34 and channel[0:2] == "PL":
                channel_hash = channel
                chan_type = "playlist"
            elif "playlist" in channel:
                chan_type = "playlist"
                channel_hash = get_playlist_hash(channel)
                if not channel_hash:
                    print("[-] Playlist '{0}' doesn't exist!".format(channel))
                    continue
            else:
                chan_type = "channel"
                channel_hash = get_channel_hash(channel)
                if not channel_hash:
                    print("[-] Channel '{0}' doesn't exist!".format(channel))
                    continue

            if chan_type == "channel":
                url = "https://www.youtube.com/channel/" + channel_hash + "/videos?spf=navigate-back"
                newest_vid = get_channel_videos(url)[0:11]
                channel_info += ";" + channel_hash + " " + newest_vid
                print("[+] Adding channel: {0}...".format(channel_hash))
            else:
                url = "https://www.youtube.com/playlist?list=" + channel_hash
                newest_vid = get_playlist_videos(url)[0:11]
                channel_info += ";" + channel_hash + " " + newest_vid
                print("[+] Adding playlist: {0}...".format(channel_hash))

        subfile.write(channel_info)
        subfile.truncate()
        print("[+] New channels have been added to subbox!")


if __name__ == "__main__":
    path = os.path.dirname(os.path.realpath(__file__)) + "/"

    parser = argparse.ArgumentParser()
    parser.add_argument("--sub-file", nargs=1,   help="Subscription info file")
    parser.add_argument("--sub",      nargs="+", help="Channel/playlist hash or url")
    parser.add_argument("--unsub",    nargs="+", help="Channel/playlist hash or url")
    args = parser.parse_args()

    if args.sub_file and os.path.isfile(args.sub_file[0]):
        subfile = path + args.sub_file[0]
        if args.sub:
            sub(subfile, args.sub)
        elif args.unsub:
            unsub(subfile, args.unsub)
        else:
            check_subbox_status(subfile)
    else:
        print("[-] Subscription info file missing, aborting...")
        print("[-] Usage: {0} --sub-file <subscription file> <optional arguments>".format(sys.argv[0]))
        exit(0)
